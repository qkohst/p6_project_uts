**After cloning the project, run the following command:**

```
$ cordova platform add android
$ cordova plugin add cordova-plugin-device
```

**Then you can use the following command to run the program**

```
$ phonegap serve
```

**Preview**

![WhatsApp_Image_2020-11-14_at_12.58.17_PM](/uploads/d0743c559b1a0a6cf55e49ab54e80387/WhatsApp_Image_2020-11-14_at_12.58.17_PM.jpeg)

![WhatsApp_Image_2020-11-14_at_12.58.17_PM__1_](/uploads/4f5efca934fb1258084bc144c483c37a/WhatsApp_Image_2020-11-14_at_12.58.17_PM__1_.jpeg)

![WhatsApp_Image_2020-11-14_at_12.58.17_PM__2_](/uploads/aa285a74666b020435e0aaaa52fc8f4d/WhatsApp_Image_2020-11-14_at_12.58.17_PM__2_.jpeg)

